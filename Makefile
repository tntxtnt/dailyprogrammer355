all: main

main: main.cpp
	g++ -std=c++1z -Wall -O2 -DFMT_HEADER_ONLY $^ -o $@ -lglpk -lm -s

.PHONY: all
