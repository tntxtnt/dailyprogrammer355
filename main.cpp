/* [2018-03-28] Challenge #355 [Intermediate] Possible Number of Pies
 *
 * Original link: https://redd.it/87rz8c
 * Vietnamese link: https://daynhauhoc.com/t/challenge-lam-banh-cho-le-ta-on/66409
 */
#include <iostream>
#include <vector>
#include <string>
#include <stdexcept>
#include <glpk.h>
#include "fmt/printf.h"

/* Solve linear programming problem
 *   Maximize cᵀx
 *   subject to: Ax ≤ b, x ≥ 0,
 *   such that the matrix A and the vectors b and c are non-negative.
 */
std::vector<int> packingLP(
    const std::vector<std::vector<double>>& A,
    const std::vector<double>& b,
    const std::vector<double>& c);

int main()
{
    std::vector<std::string> pieNames = {
        "pumpkin pie",
        "apple pie",
    };
    std::vector<std::vector<double>> pieRecipes = {
    //pumpkin pie    apple pie
        {  1      ,      0  },  //scoop of synthetic pumpkin flavouring
        {  0      ,      1  },  //apple
        {  3      ,      4  },  //eggs
        {  4      ,      3  },  //cups of milk
        {  3      ,      2  },  //cups of sugar
    };
    std::vector<double> pieBias = {1, 1}; //1-1: no bias


    std::vector<std::vector<double>> ingredients = {
    //  pump appl eggs milk sugr
        {10,  14,  10,  42,  24},
        {12,   4,  40,  30,  40},
        {12,  14,  20,  42,  24},
    };
    for (auto const& ing : ingredients)
    {
        auto result = packingLP(pieRecipes, ing, pieBias);
        fmt::print("{} {}{} and {} {}{}\n",
            result[0], pieNames[0], result[0] == 1 ? "" : "s",
            result[1], pieNames[1], result[1] == 1 ? "" : "s");
    }
}


std::vector<int> packingLP(
    const std::vector<std::vector<double>>& A,
    const std::vector<double>& b,
    const std::vector<double>& c)
{
    int rows = A.size();
    int cols = A[0].size();
    // init packing LP
    glp_prob* lp = glp_create_prob();
    glp_set_obj_dir(lp, GLP_MAX);
    glp_add_rows(lp, rows);
    glp_add_cols(lp, cols);
    // load b
    for (int i = 0; i < rows; ++i)
        glp_set_row_bnds(lp, i + 1, GLP_UP, 0.0, b[i]);
    // load c
    for (int j = 0; j < cols; ++j)
    {
        glp_set_col_bnds(lp, j + 1, GLP_LO, 0.0, 0.0);
        glp_set_obj_coef(lp, j + 1, c[j]);
    }
    // load A
    int aSize = rows * cols;
    std::vector<int> ia(aSize + 1);
    std::vector<int> ja(aSize + 1);
    std::vector<double> va(aSize + 1);
    for (int i = 0, id = 1; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            ia[id] = i + 1;
            ja[id] = j + 1;
            va[id] = A[i][j];
            id++;
        }
    }
    glp_load_matrix(lp, aSize, &ia[0], &ja[0], &va[0]);
    // set MIP columns
    for (int j = 0; j < cols; ++j)
        glp_set_col_kind(lp, j + 1, GLP_IV);
    // solve MIP
    glp_iocp iocp;
    glp_init_iocp(&iocp);
    iocp.msg_lev = GLP_MSG_OFF;
    iocp.presolve = GLP_ON;
    int err = glp_intopt(lp, &iocp);
    if (err) throw std::runtime_error("Error during `glp_intopt()`");
    // get x
    std::vector<int> x(cols);
    for (int j = 0; j < cols; ++j)
        x[j] = glp_mip_col_val(lp, j + 1);
    glp_delete_prob(lp);
    return x;
}
